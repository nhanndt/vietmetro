package com.nhanndt.vietmetro;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.util.Xml;
import android.view.Menu;
import android.view.MenuItem;

import com.nhanndt.vietmetro.data.Connection;
import com.nhanndt.vietmetro.data.Line;
import com.nhanndt.vietmetro.data.MetroDBOpenHelper;
import com.nhanndt.vietmetro.data.MetroDatabaseContract;
import com.nhanndt.vietmetro.data.Station;
import com.nhanndt.vietmetro.search.PathFinder;
import com.nhanndt.vietmetro.utils.ImageTool;
import com.nhanndt.vietmetro.utils.NLog;
import com.nhanndt.vietmetro.utils.TouchMetroMapView;
import com.nhanndt.vietmetro.utils.TouchMetroMapView.OnStationTouchListener;

public class MainActivity extends ActionBarActivity {
	
	TouchMetroMapView mainMapView = null;
	MetroDBOpenHelper mMetroDBOpenHelper = null;
	Station startStn = null;
	Station endStn = null;
	int updateStage = 0;
	String dbName = "";
	
	//
	private ActionBar mActionBar = null;
	private ProgressDialog mProgressDialog = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		init();
		loadingDatabase();
	}

	private void init(){
		mActionBar = getSupportActionBar();
		mActionBar.setLogo(R.drawable.ic_launcher);
		mActionBar.setDisplayShowHomeEnabled(true);
		mActionBar.setDisplayUseLogoEnabled(true);
		mActionBar.setTitle(getResources().getString(R.string.app_name));
		//mActionBar.setDisplayShowTitleEnabled(true);
		//
		if(mainMapView == null)
			mainMapView = (TouchMetroMapView)findViewById(R.id.main_map_view);
		mainMapView.setOnStationTouchListener(onStationTouchListener);
		switch (VietMetro.getCurrentCityID(getApplicationContext())) {
		case VietMetro.ID_HANOI:
			dbName = MetroDatabaseContract.DATABASE_NAME_HANOI;
			break;
		case VietMetro.ID_DANANG:
			//dbName = MetroDatabaseContract.DATABASE_NAME_DANANG;
			break;
		case VietMetro.ID_HCM:
			dbName = MetroDatabaseContract.DATABASE_NAME_HCM;
			break;
		}
		if(mMetroDBOpenHelper == null){
			mMetroDBOpenHelper = new MetroDBOpenHelper(getApplicationContext(), MetroDatabaseContract.DATABASE_NAME_HCM);
		}
	}
	
	private void loadingDatabase(){
		mMetroDBOpenHelper.openDB();
		mProgressDialog = ProgressDialog.show(this, "", "Loading ...");
		LoadingDatabase loadingDatabaseTask = new LoadingDatabase("StationData.xml");
		loadingDatabaseTask.execute();
		loadingDatabaseTask = new LoadingDatabase("LineData.xml");
		loadingDatabaseTask.execute();
		loadingDatabaseTask = new LoadingDatabase("ConnectionData.xml");
		loadingDatabaseTask.execute();
	}
	
	@Override
	public void onWindowFocusChanged(boolean hasFocus){
		//loadingMapData(VietMetro.getCurrentCityID(getApplicationContext()));
	}
	@Override
	public void onBackPressed(){
		//super.onBackPressed();
		if(mainMapView.isDrawn())
			mainMapView.reset();
		else
			finish();
	}
	
	private void loadingMapData(int cityID){
		Bitmap mainBM = null;
		switch (cityID) {
		case VietMetro.ID_HANOI:
			mainBM = ImageTool.decodeBitmapFromResource(getResources(), R.drawable.hanoi_fullmap_1, mainMapView.getWidth(), mainMapView.getHeight());
			break;
		case VietMetro.ID_HCM:
			mainBM = ImageTool.decodeBitmapFromResource(getResources(), R.drawable.hcm_subway_map_png, mainMapView.getWidth(), mainMapView.getHeight());
			break;
		case VietMetro.ID_DANANG:
			return;
		}
		mainMapView.setImageBitmap(mainBM);
	}

	TouchMetroMapView.OnStationTouchListener onStationTouchListener = new OnStationTouchListener() {
		
		@Override
		public void onStationTouch(Station stn) {
			// TODO Auto-generated method stub
			showDialog(stn);
		}
		
		@Override
		public void onIntersectionTouch() {
			// TODO Auto-generated method stub
			
		}
	};
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		//implement SearchView
		SearchManager searchManager = (SearchManager)getSystemService(Context.SEARCH_SERVICE);
		//SearchView searchView = (SearchView)menu.findItem(R.id.action_search).getActionView();
		SearchView searchView = (SearchView)MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
		searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
		searchView.setOnQueryTextListener(new OnQueryTextListener() {
			
			@Override
			public boolean onQueryTextSubmit(String arg0) {
				// TODO Auto-generated method stub
				return true;
			}
			
			@Override
			public boolean onQueryTextChange(String arg0) {
				// TODO Auto-generated method stub
				return true;
			}
		});
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
//		if (id == R.id.hanoi_item) {
//			VietMetro.setCurrentCityID(getApplicationContext(), VietMetro.ID_HANOI);
//			dbName = MetroDatabaseContract.DATABASE_NAME_HANOI;
//		} else if (id == R.id.danang_item){
////			VietMetro.setCurrentCityID(getApplicationContext(), VietMetro.ID_DANANG);
////			dbName = MetroDatabaseContract.DATABASE_NAME_DANANG;
//		} else
//			if (id == R.id.hcm_item){
//			VietMetro.setCurrentCityID(getApplicationContext(), VietMetro.ID_HCM);
//			dbName = MetroDatabaseContract.DATABASE_NAME_HCM;
//		}
//		loadingMapData(VietMetro.getCurrentCityID(getApplicationContext()));
		return super.onOptionsItemSelected(item);
	}
	//
	class LoadingDatabase extends AsyncTask<Void, Void, Void>{
		
		private String xmlFile = null;
		
		LoadingDatabase(){}
		LoadingDatabase(String filename){
			this.xmlFile = filename;
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			if(xmlFile!=null){
				InputStream in = null;            
				try {
					in = getApplicationContext().getAssets().open(xmlFile);
					if(in == null)
						NLog.e("Input stream is NULL");
					XmlPullParser parser = Xml.newPullParser();
					parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
					parser.setInput(in, null);
					parseXML(parser);
				} catch (XmlPullParserException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} finally {
					try {
						in.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}			
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result){
			super.onPostExecute(result);
			updateStage++;
			if(updateStage == 3){
				loadingMapData(VietMetro.getCurrentCityID(getApplicationContext()));
				mMetroDBOpenHelper.closeDB();
				if(mProgressDialog.isShowing())
					mProgressDialog.dismiss();
			}
		}
		//
		private void parseXML(XmlPullParser parser) throws XmlPullParserException,IOException {
			int eventType = parser.getEventType();
			Station station = null;
			Line line = null;
			Connection connection = null;
			while(eventType != XmlPullParser.END_DOCUMENT){
				String tagName = null;
				switch (eventType) {
				case XmlPullParser.START_DOCUMENT:
					tagName = parser.getName();
					break;
				case XmlPullParser.START_TAG:
					tagName = parser.getName();
					if(tagName.equals("stations")){
						NLog.e("========== Updating Station ==========");
					} else if (tagName.equals("lines")){
						NLog.e("========== Updating Line ==========");
					} else if (tagName.equals("connections")){
						NLog.e("========== Updating Connection ==========");
					}
					if(tagName.equals("station")){
						NLog.i("******* new station *******");
						station = new Station();
					}
					if(tagName.equals("line")){
						NLog.i("******* new line *******");
						line = new Line();
					}
					if(tagName.equals("connection")){
						NLog.i("******* new connection *******");
						connection = new Connection();
					}
					//
					if(station != null){
						if(tagName.equals("id")){
							eventType = parser.next();
							if(eventType == XmlPullParser.TEXT)
								station.stnID = parser.getText();
						}
						if(tagName.equals("name")){
							eventType = parser.next();
							if(eventType == XmlPullParser.TEXT)
								station.name = parser.getText();
						}
						if(tagName.equals("x_coordinate")){
							eventType = parser.next();
							if(eventType == XmlPullParser.TEXT)
								station.x = Float.parseFloat(parser.getText());
						}
						if(tagName.equals("y_coordinate")){
							eventType = parser.next();
							if(eventType == XmlPullParser.TEXT)
								station.y = Float.parseFloat(parser.getText());
						}
					}
					//
					if(line != null){
						if(tagName.equals("id")){
							eventType = parser.next();
							if(eventType == XmlPullParser.TEXT)
								line.lineID = parser.getText();
						}
						if(tagName.equals("name")){
							eventType = parser.next();
							if(eventType == XmlPullParser.TEXT)
								line.name = parser.getText();
						}
						if(tagName.equals("color")){
							eventType = parser.next();
							if(eventType == XmlPullParser.TEXT)
								line.color = parser.getText();
						}
						if(tagName.equals("length")){
							eventType = parser.next();
							if(eventType == XmlPullParser.TEXT)
								line.length = Float.parseFloat(parser.getText());
						}
					}
					//
					if(connection != null){
						if(tagName.equals("id")){
							eventType = parser.next();
							if(eventType == XmlPullParser.TEXT)
								connection.connectionID = parser.getText();
						}
						if(tagName.equals("station_id1")){
							eventType = parser.next();
							if(eventType == XmlPullParser.TEXT)
								connection.stnID1 = parser.getText();
						}
						if(tagName.equals("station_id2")){
							eventType = parser.next();
							if(eventType == XmlPullParser.TEXT)
								connection.stnID2 = parser.getText();
						}
						if(tagName.equals("line_id")){
							eventType = parser.next();
							if(eventType == XmlPullParser.TEXT)
								connection.lineID = parser.getText();
						}
						if(tagName.equals("length")){
							eventType = parser.next();
							if(eventType == XmlPullParser.TEXT)
								connection.length = Float.parseFloat(parser.getText());
						}
						if(tagName.equals("time")){
							eventType = parser.next();
							if(eventType == XmlPullParser.TEXT)
								connection.time = Float.parseFloat(parser.getText());
						}
					}
					break;
				case XmlPullParser.END_TAG:
					tagName = parser.getName();
					if(tagName.equals("station")){
						mMetroDBOpenHelper.insertStation(station);
					}
					if(tagName.equals("line")){
						mMetroDBOpenHelper.insertLine(line);
					}
					if(tagName.equals("connection")){
						mMetroDBOpenHelper.insertConnection(connection);
					}
					NLog.i("/" + tagName);
				}
				eventType = parser.next();
			}
		}
	}
	//
	//
	private void showDialog(final Station stn){
		final String options[] = getResources().getStringArray(R.array.station_option_array);
		AlertDialog.Builder builder = new Builder(this);
		builder.setTitle(stn.name);
		builder.setCancelable(true);
		builder.setItems( options, new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				switch(which){
				case 0:
					startStn = stn;
					mainMapView.drawStartPoint(startStn.x, startStn.y);
					break;
				case 1:
					endStn = stn;
					mainMapView.drawDestinationPoint(endStn.x, endStn.y);
					break;
				case 2:
				}
				//
				if(mainMapView.isSearchable()){
					findPath();
				}
			}
		});
		builder.setNegativeButton(R.string.cancel_btn_string, null);
		builder.create().show();
	}
	//
	private void findPath(){
		mProgressDialog = ProgressDialog.show(this, null, "Searching ...");
		AsyncTask<Void, Void, ArrayList<Station>> findPathTask = new AsyncTask<Void, Void, ArrayList<Station>>(){
			@Override
			protected ArrayList<Station> doInBackground(Void... params) {
				// TODO Auto-generated method stub
				PathFinder mPathFinder = new PathFinder(getApplicationContext(), startStn, endStn, dbName);
				NLog.e("db Name: " + dbName);
				return mPathFinder.getShortestPath();
			}
			
			@Override
			protected void onPostExecute(ArrayList<Station> result){
				super.onPostExecute(result);
				mainMapView.drawPath(result);
				if(mProgressDialog.isShowing())
					mProgressDialog.dismiss();
			}
			
		};
		findPathTask.execute();
	}
}
