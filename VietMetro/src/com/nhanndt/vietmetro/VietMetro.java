package com.nhanndt.vietmetro;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;

public class VietMetro extends Application {
	public static final String PACKAGE_NAME = "com.nhanndt.vietmetro";
	public static final String PACKAGE_PATH = Environment.getExternalStorageDirectory() + "/VietMetro";
	public static final String DEFAULT_PREF_NAME = "VietMetro_Settings";
	public static final String ID_CURRENT = "id_current";
	public static final int ID_HANOI = 1;
	public static final int ID_DANANG = 2;
	public static final int ID_HCM = 3;
	public static final int STATION_RADIUS = 30;
	public static final int ORIGINAL_MAP_WIDTH = 1282;
	public static final int ORIGINAL_MAP_HEIGHT = 1085;
	public static int getCurrentCityID(Context context){
		SharedPreferences settings = context.getSharedPreferences(DEFAULT_PREF_NAME, Context.MODE_PRIVATE);
		return settings.getInt(ID_CURRENT, ID_HCM);
	}
	//
	public static void setCurrentCityID(Context context, int id){
		SharedPreferences settings = context.getSharedPreferences(DEFAULT_PREF_NAME, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt(ID_CURRENT, id);
		editor.commit();
	}
}
