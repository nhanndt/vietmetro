package com.nhanndt.vietmetro.data;

public class Line {
	public interface Color {
		String RED = "RED";
		String GREEN = "GREEN";
		String BLUE = "BLUE";
		String PURPLE = "PURPLE";
		String YELLOW = "YELLOW";
		String ORANGE = "ORANGE";
	}
	
	public String lineID;
	public String name;
	public String color;
	public float length;
	
	public Line(){}
}
