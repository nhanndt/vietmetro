package com.nhanndt.vietmetro.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.nhanndt.vietmetro.data.MetroDatabaseContract.LineTable;
import com.nhanndt.vietmetro.data.MetroDatabaseContract.StationConnectionTable;
import com.nhanndt.vietmetro.data.MetroDatabaseContract.StationTable;
import com.nhanndt.vietmetro.utils.NLog;

public class MetroDBOpenHelper extends SQLiteOpenHelper{
	
	private SQLiteDatabase readDB = null;
	private SQLiteDatabase writeDB = null;
	private Context mContext = null;
	private String dbName = "";

	public MetroDBOpenHelper(Context context, String dbName){
		super(context, dbName, null, MetroDatabaseContract.DATABASE_VERSION);
		this.mContext = context;
		readDB = getReadableDatabase();
		writeDB = getWritableDatabase();
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL(MetroDatabaseContract.StationTable._CREATE);
		db.execSQL(MetroDatabaseContract.LineTable._CREATE);
		db.execSQL(MetroDatabaseContract.StationConnectionTable._CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		db.execSQL("DROP TABLE IF EXISTS '" + MetroDatabaseContract.StationTable.TABLE_NAME + "'");
		db.execSQL("DROP TABLE IF EXISTS '" + MetroDatabaseContract.LineTable.TABLE_NAME + "'");
		db.execSQL("DROP TABLE IF EXISTS '" + MetroDatabaseContract.StationConnectionTable.TABLE_NAME + "'");
        onCreate(db);
	}
	
	/**
	 * Query Database methods
	 */
	public void closeDB(){
		if(readDB != null)
			readDB.close();
		if(writeDB != null)
			writeDB.close();
	}
	
	public void openDB(){
		if(!readDB.isOpen())
			readDB = getReadableDatabase();
		if(!writeDB.isOpen())
			writeDB = getWritableDatabase();
	}

	public void insertStation(Station stn){
		NLog.i("stnID: " + stn.stnID);
		NLog.i("name: " + stn.name);
		NLog.i("x: " + stn.x);
		NLog.i("y: " + stn.y);
		//
		Cursor cursor = getStationByID(stn.stnID);
		if(cursor.getCount() > 0){
			NLog.e("stnID: " + stn.stnID);
			NLog.e("name: " + stn.name);
			NLog.e("ERROR: station exists!");
		}
		else {
			ContentValues values = new ContentValues();
			values.put(StationTable.COLUMN_STATION_ID, stn.stnID);
			values.put(StationTable.COLUMN_STATION_NAME, stn.name);
			values.put(StationTable.COLUMN_STATION_X, stn.x);
			values.put(StationTable.COLUMN_STATION_Y, stn.y);
			writeDB.insert(StationTable.TABLE_NAME, null, values);
			NLog.i("inserted!");
		}
	}
	
	public void insertLine(Line line){
		NLog.i("lineID: " + line.lineID);
		NLog.i("name: " + line.name);
		NLog.i("color: " + line.color);
		NLog.i("length: " + line.length);
		//
		Cursor cursor = getLineByID(line.lineID);
		if(cursor.getCount() > 0)
			NLog.e("ERROR: line exists!");
		else {
			ContentValues values = new ContentValues();
			values.put(LineTable.COLUMN_LINE_ID, line.lineID);
			values.put(LineTable.COLUMN_LINE_NAME, line.name);
			values.put(LineTable.COLUMN_LINE_COLOR, line.color);
			values.put(LineTable.COLUMN_LINE_LENGTH, line.length);
			writeDB.insert(LineTable.TABLE_NAME, null, values);
			NLog.i("inserted!");
		}
	}
	
	public void insertConnection(Connection connection){
		NLog.i("connectionID: " + connection.connectionID);
		NLog.i("stnID1: " + connection.stnID1);
		NLog.i("stnID2: " + connection.stnID2);
		NLog.i("lineID: " + connection.lineID);
		NLog.i("length: " + connection.length);
		NLog.i("time: " + connection.time);
		//
		Cursor cursor = getConnectionByID(connection.connectionID);
		if(cursor.getCount() > 0)
			NLog.e("ERROR: connection exists!");
		else {
			ContentValues values = new ContentValues();
			values.put(StationConnectionTable.COLUMN_CONNECTION_ID, connection.connectionID);
			values.put(StationConnectionTable.COLUMN_STATION_ID1, connection.stnID1);
			values.put(StationConnectionTable.COLUMN_STATION_ID2, connection.stnID2);
			values.put(StationConnectionTable.COLUMN_LINE_ID, connection.lineID);
			values.put(StationConnectionTable.COLUMN_CONNECTION_LENGTH, connection.length);
			values.put(StationConnectionTable.COLUMN_CONNECTION_TIME, connection.time);
			writeDB.insert(StationConnectionTable.TABLE_NAME, null, values);
			NLog.i("inserted!");
		}
	}
	
	public Cursor loadAllStation(){
		Cursor out = readDB.query(MetroDatabaseContract.StationTable.TABLE_NAME, MetroDatabaseContract.StationTable.STATION_ALL_COLUMN,
							null, null, null, null, null);
		return out;
	}
	
	public Cursor loadAllLine(){
		Cursor out = readDB.query(MetroDatabaseContract.LineTable.TABLE_NAME, MetroDatabaseContract.LineTable.LINE_ALL_COLUMN,
							null, null, null, null, null);
		return out;
	}
	
	public Cursor loadAllConnection(){
		Cursor out = readDB.query(MetroDatabaseContract.LineTable.TABLE_NAME, MetroDatabaseContract.LineTable.LINE_ALL_COLUMN,
							null, null, null, null, null);
		return out;
	}
	
	public Cursor loadAllStationPosition(){
		Cursor out = readDB.query(MetroDatabaseContract.StationTable.TABLE_NAME, new String[] {MetroDatabaseContract.StationTable.COLUMN_STATION_X, MetroDatabaseContract.StationTable.COLUMN_STATION_Y},
							null, null, null, null, null);
		return out;
	}
	
	public Cursor getStationByID(String ID){
		String where = MetroDatabaseContract.StationTable.COLUMN_STATION_ID +"='" + ID + "'";
		Cursor out = readDB.query(MetroDatabaseContract.StationTable.TABLE_NAME, MetroDatabaseContract.StationTable.STATION_ALL_COLUMN,
				where, null, null, null, null);
		return out;
	}
	
	public Cursor getLineByID(String ID){
		String where = MetroDatabaseContract.LineTable.COLUMN_LINE_ID +"='" + ID + "'";
		Cursor out = readDB.query(MetroDatabaseContract.LineTable.TABLE_NAME, MetroDatabaseContract.LineTable.LINE_ALL_COLUMN,
				where, null, null, null, null);
		return out;
	}
	
	public Cursor getConnectionByID(String ID){
		String where = MetroDatabaseContract.StationConnectionTable.COLUMN_CONNECTION_ID +"='" + ID + "'";
		Cursor out = readDB.query(MetroDatabaseContract.StationConnectionTable.TABLE_NAME, MetroDatabaseContract.StationConnectionTable.CONNECTION_ALL_COLUMN,
				where, null, null, null, null);
		return out;
	}
	
	public Cursor getConnectionByStationID(String stnID){
		String where = MetroDatabaseContract.StationConnectionTable.COLUMN_STATION_ID1 +" ='" + stnID + "' OR " + 
					   MetroDatabaseContract.StationConnectionTable.COLUMN_STATION_ID2 +" ='" + stnID + "'";
		Cursor out = readDB.query(MetroDatabaseContract.StationConnectionTable.TABLE_NAME, MetroDatabaseContract.StationConnectionTable.CONNECTION_ALL_COLUMN,
				where, null, null, null, null);
		return out;
	}
	
	
}
