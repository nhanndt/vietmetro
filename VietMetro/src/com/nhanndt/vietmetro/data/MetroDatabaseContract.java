package com.nhanndt.vietmetro.data;

import android.provider.BaseColumns;

public final class MetroDatabaseContract {
	
	public static final String DATABASE_NAME_HCM = "vietmetro_hcm.db";
	public static final String DATABASE_NAME_HANOI = "vietmetro_hanoi.db";
	public static final String DATABASE_NAME_DANANG = "vietmetro_danang.db";
	public static final int DATABASE_VERSION = 1;
	
	public static final class StationTable implements BaseColumns{
		public static final String TABLE_NAME = "station";
		public static final String COLUMN_STATION_NAME = "name";
		public static final String COLUMN_STATION_ID = "stationID";
		public static final String COLUMN_STATION_X = "positionX";
		public static final String COLUMN_STATION_Y = "positionY";
		public static final String STATION_ALL_COLUMN[] = {_ID,
														   COLUMN_STATION_ID,
														   COLUMN_STATION_NAME,
														   COLUMN_STATION_X,
														   COLUMN_STATION_Y};
		//
		public static final String _CREATE = "create table " + TABLE_NAME + "("
		+ _ID + " integer primary key autoincrement, " 
		+ COLUMN_STATION_NAME + " text not null , "
		+ COLUMN_STATION_ID + " text not null , "
		+ COLUMN_STATION_X + " real , "
		+ COLUMN_STATION_Y + " real);"; 
	}
	//
	public static final class LineTable implements BaseColumns{
		public static final String TABLE_NAME = "line";
		public static final String COLUMN_LINE_NAME = "name";
		public static final String COLUMN_LINE_ID = "lineID";
		public static final String COLUMN_LINE_COLOR = "color";
		public static final String COLUMN_LINE_LENGTH = "length";
		public static final String LINE_ALL_COLUMN[] = {_ID,
														COLUMN_LINE_NAME,
														COLUMN_LINE_ID,
														COLUMN_LINE_COLOR,
														COLUMN_LINE_LENGTH};
		//
		public static final String _CREATE = "create table " + TABLE_NAME + "("
		+ _ID + " integer primary key autoincrement, " 
		+ COLUMN_LINE_NAME + " text not null , "
		+ COLUMN_LINE_ID + " text not null , "
		+ COLUMN_LINE_LENGTH + " real , "
		+ COLUMN_LINE_COLOR + " text not null );"; 
	}
	//
	public static final class StationConnectionTable implements BaseColumns{
		public static final String TABLE_NAME = "stationconnection";
		public static final String COLUMN_CONNECTION_ID = "connectionID";
		public static final String COLUMN_STATION_ID1 = "stationID1";
		public static final String COLUMN_STATION_ID2 = "stationID2";
		public static final String COLUMN_LINE_ID = "lineID";
		public static final String COLUMN_CONNECTION_LENGTH = "length";
		public static final String COLUMN_CONNECTION_TIME = "time";
		public static final String CONNECTION_ALL_COLUMN[] = {_ID,
														COLUMN_CONNECTION_ID,
														COLUMN_STATION_ID1,
														COLUMN_STATION_ID2,
														COLUMN_LINE_ID,
														COLUMN_CONNECTION_LENGTH,
														COLUMN_CONNECTION_TIME};
		//
		public static final String _CREATE = "create table " + TABLE_NAME + "("
		+ _ID + " integer primary key autoincrement, " 
		+ COLUMN_CONNECTION_ID + " text not null , "
		+ COLUMN_STATION_ID1 + " text not null , "
		+ COLUMN_STATION_ID2 + " text not null , "
		+ COLUMN_LINE_ID + " text not null , "
		+ COLUMN_CONNECTION_LENGTH + " real not null , "
		+ COLUMN_CONNECTION_TIME + " real not null ); "; 
	}
}
