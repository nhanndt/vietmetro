package com.nhanndt.vietmetro.data;

public class Station {
	
	public float x;
	public float y;
	public String stnID;
	public String name;
	
	public Station(){}
	public Station(String id){this.stnID = id;}
}
