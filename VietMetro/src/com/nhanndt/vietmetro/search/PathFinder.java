package com.nhanndt.vietmetro.search;

import java.util.ArrayList;
import java.util.Iterator;

import android.content.Context;
import android.database.Cursor;

import com.nhanndt.vietmetro.data.Connection;
import com.nhanndt.vietmetro.data.MetroDBOpenHelper;
import com.nhanndt.vietmetro.data.MetroDatabaseContract;
import com.nhanndt.vietmetro.data.Station;
import com.nhanndt.vietmetro.utils.NLog;

public class PathFinder {
	public static final float D_VALUE_MAX = 10000;
	public static final float TRANSFER_D_VALUE = 2.5f;
	
	/**
	 * Database name.
	 */
	private String DBName = "";
	
	/**
	 * The application Context.
	 */
	private Context mContext = null;
	
	/**
	 * The departure station.
	 */
	private Station startNode = null;
	
	/**
	 * The destination station.
	 */
	private Station endNode = null;
	
	/**
	 * checked List in Dijkstra's Algorithm.
	 */
	private ArrayList<DValue> stationDValueList = null;
	private ArrayList<Station>[] trace = null;
	
	/**
	 * Default constructor.
	 */
	public PathFinder(Context context, String dbName){
		this.mContext = context;
		this.stationDValueList = new ArrayList<PathFinder.DValue>();
		this.DBName = dbName;
	}
	
	/**
	 * PathFinder constructor.
	 * @param start
	 * 		The departure station.
	 * @param end
	 * 		The destination station.
	 */
	public PathFinder(Context context, Station start, Station end, String dbName){
		this.mContext = context;
		this.startNode = start;
		this.endNode = end;
		this.stationDValueList = new ArrayList<PathFinder.DValue>();
		this.DBName = dbName;
	}
	
	/**
	 * Set Start Station for Path Finder object
	 * @param start
	 * 		Start {@code Station}.
	 */
	public void setStartStation(Station start){
		this.startNode = start;
	}
	
	/**
	 * Set End Station for Path Finder object.
	 * @param end
	 * 		End {@code Station}.
	 */
	public void setEndStation(Station end){
		this.endNode = end;
	}
	
	/**
	 * Find shortest path from Start node to End node by Dijkstra's algorithm.
	 * @return
	 */
	public ArrayList<Station> getShortestPath(){
		if(startNode == null || endNode == null)
			return null;
		ArrayList<Connection> neighbor = null;
		Station crrtStation = startNode;
		setStationDValue(crrtStation.stnID, 0, "", crrtStation.stnID);
		boolean found = false;
		while((!found) && !isAllVisited()){
			//
			if(crrtStation.stnID.equals(endNode.stnID)){
				found = true;
				NLog.e("Found Path: " + crrtStation.stnID + " <-- VALUE: " + getStationDValue(crrtStation.stnID).value);
				break;
			}
			//
			neighbor = getNeighborConnections(crrtStation.stnID);
			if(neighbor == null){
				break;
			}
			NLog.e("==============================");
			NLog.e("Visited station: " + crrtStation.stnID);
			for(int i=0; i<neighbor.size(); i++){
				String neighborID;
				if(crrtStation.stnID.equals(neighbor.get(i).stnID1))
					neighborID = neighbor.get(i).stnID2;
				else
					neighborID = neighbor.get(i).stnID1;
				if(isVisited(neighborID))
					continue;
				NLog.i("neighbor ID: " + neighborID);
				NLog.i("DValue: " + getStationDValue(neighborID).value);
				DValue cDValue = getStationDValue(crrtStation.stnID);
				if((cDValue.value + neighbor.get(i).length + ((cDValue.lineID.equals(neighbor.get(i).lineID) || cDValue.lineID.equals("")) ? 0:TRANSFER_D_VALUE)) < getStationDValue(neighborID).value)
					setStationDValue(neighborID, cDValue.value + neighbor.get(i).length + ((cDValue.lineID.equals(neighbor.get(i).lineID) || cDValue.lineID.equals("")) ? 0:TRANSFER_D_VALUE), neighbor.get(i).lineID, crrtStation.stnID);
				NLog.i("New DValue: " + getStationDValue(neighborID).value);
			}
			//
			removeVisitednode(crrtStation.stnID);
			crrtStation = new Station(getMinDValueStation());
		}
		// Tracking back rout
		if(!found)
			return null;
		else{
			ArrayList<Station> mPath = new ArrayList<Station>();
			String stn = crrtStation.stnID;
			mPath.add(crrtStation);
			while(!stn.equals(startNode.stnID)){
				if(stn.equals(""))
					break;
				DValue dValue = getStationDValue(stn);
				stn = dValue.preStationID;
				Station tStn = new Station(stn);
				mPath.add(tStn);
			}
			return mPath;
		}		
	}
	
	/**
	 * Find all Neighbor connections of the input station.
	 */
	private ArrayList<Connection> getNeighborConnections(String id){
		MetroDBOpenHelper mMetroDBOpenHelper = new MetroDBOpenHelper(mContext, DBName);
		mMetroDBOpenHelper.openDB();
		Cursor cursor = mMetroDBOpenHelper.getConnectionByStationID(id);
		if(cursor.getCount() == 0){
			mMetroDBOpenHelper.closeDB();
			return null;
		} else {
			ArrayList<Connection> neighbor = new ArrayList<Connection>();
			boolean move = cursor.moveToFirst();
			while(move){
				Connection connection = new Connection();
				connection.connectionID = cursor.getString(cursor.getColumnIndex(MetroDatabaseContract.StationConnectionTable.COLUMN_CONNECTION_ID));
				connection.stnID1 = cursor.getString(cursor.getColumnIndex(MetroDatabaseContract.StationConnectionTable.COLUMN_STATION_ID1));
				connection.stnID2 = cursor.getString(cursor.getColumnIndex(MetroDatabaseContract.StationConnectionTable.COLUMN_STATION_ID2));
				connection.lineID = cursor.getString(cursor.getColumnIndex(MetroDatabaseContract.StationConnectionTable.COLUMN_LINE_ID));
				connection.length = cursor.getFloat(cursor.getColumnIndex(MetroDatabaseContract.StationConnectionTable.COLUMN_CONNECTION_LENGTH));
				connection.time = cursor.getFloat(cursor.getColumnIndex(MetroDatabaseContract.StationConnectionTable.COLUMN_CONNECTION_TIME));				
				neighbor.add(connection);
				move = cursor.moveToNext();				
			}
			mMetroDBOpenHelper.closeDB();
			return neighbor;
		}
	}
	
	private class DValue{
		public String stnID;
		public String lineID = "";
		public String preStationID = "";
		public float value = D_VALUE_MAX;
		public boolean isVisited = false;
		public DValue(String id, float v, String lineID, String preStn){
			this.stnID = id;
			this.value = v;
			this.preStationID = preStn;
			this.lineID = lineID;
		}
	}
	
	private DValue getStationDValue(String id){
		for(int i=0; i<stationDValueList.size(); i++){
			if(stationDValueList.get(i).stnID.equals(id)){
				return stationDValueList.get(i);
			}
		}
		return new DValue("", D_VALUE_MAX, "", "");
	}
	
	private void setStationDValue(String id, float value, String lineID, String preStation){
		for(int i=0; i<stationDValueList.size(); i++){
			if(stationDValueList.get(i).stnID.equals(id)){
				stationDValueList.get(i).value = value;
				stationDValueList.get(i).lineID = lineID;
				stationDValueList.get(i).preStationID = preStation;
				NLog.e("Set D-Value stnID " + stationDValueList.get(i).stnID);
				NLog.e("Set D-Value lineID " + stationDValueList.get(i).lineID);
				NLog.e("Set D-Value preStationID " + stationDValueList.get(i).preStationID);
				return;
			}
		}
		stationDValueList.add(new DValue(id, value, lineID, preStation));
	}
	
	private void removeVisitednode(String id){
		for(int i=0; i<stationDValueList.size(); i++){
			if(stationDValueList.get(i).stnID.equals(id)){
				stationDValueList.get(i).isVisited = true;
				break;
			}
		}
	}
	
	private boolean isVisited(String id){
		return getStationDValue(id).isVisited;
	}
	
	private boolean isAllVisited(){
		for(int i=0; i<stationDValueList.size(); i++){
			if(!stationDValueList.get(i).isVisited){
				return false;
			}
		}
		return true;
	}
	
	private String getMinDValueStation(){
		String id = null;
		float value = D_VALUE_MAX;
		for(int i=0; i<stationDValueList.size(); i++){
			if(stationDValueList.get(i).value <= value && !stationDValueList.get(i).isVisited){
				value = stationDValueList.get(i).value;
				id = stationDValueList.get(i).stnID;
			}
		}
		return id;
	}

}
