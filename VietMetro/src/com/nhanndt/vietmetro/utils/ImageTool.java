package com.nhanndt.vietmetro.utils;

import java.lang.ref.WeakReference;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.DisplayMetrics;
import android.util.Pair;
import android.util.TypedValue;

public class ImageTool {

	public static Bitmap decodeBitmapFromFile(String filename, int reqWidth, int reqHeight){
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(filename, options);
		options.inSampleSize = optimizeImageSize(options, reqWidth, reqHeight);
		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeFile(filename, options);
	}
	
	public static Bitmap decodeBitmapFromResource(Resources res, int resourceID, int reqWidth, int reqHeight){
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inMutable = true;
		options.inJustDecodeBounds = true;
		NLog.i("options.outWidth: " + options.outWidth);
		NLog.i("options.outHeight: " + options.outHeight);
		BitmapFactory.decodeResource(res, resourceID, options);
		NLog.i("options.outWidth: " + options.outWidth);
		NLog.i("options.outHeight: " + options.outHeight);
		NLog.i("reqWidth: " + reqWidth);
		NLog.i("reqHeight: " + reqHeight);
		options.inSampleSize = optimizeImageSize(options, reqWidth, reqHeight);
		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeResource(res, resourceID, options);
	}
	
	private static int optimizeImageSize(BitmapFactory.Options options, int reqWidth, int reqHeight){
		int width = options.outWidth;
		int height = options.outHeight;
		int inSampleSize = 1;
		width /= 2;
		height /= 2;
		while (height > reqHeight && width > reqWidth) {
			inSampleSize *= 2;
			width /= 2;
			height /= 2;
//			final int halfHeight = height / 2;
//			final int halfWidth = width / 2;
//			while ((halfHeight / inSampleSize) > reqHeight
//					&& (halfWidth / inSampleSize) > reqWidth) {
//				inSampleSize *= 2;
//			}
		}
		return inSampleSize;
	}
	
	private void optimizedBitmap(){
		//Pair<Path, S>
	}
	
	public static float dpToPx(float dp, Context con) {
		float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, con.getResources().getDisplayMetrics());
		return px;
	}
}
