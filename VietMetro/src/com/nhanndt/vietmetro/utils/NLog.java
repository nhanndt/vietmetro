package com.nhanndt.vietmetro.utils;

import android.util.Log;

/**
 * Output internal Log for DEBUG.
 * 
 * @author Nhan Nguyen
 *
 */

public class NLog {
	
	private static final String TAG = "TIMPHONGTRO";
	
	private static void print(String tag, int method, String caller, String msg){
		switch (method) {
		case Log.DEBUG:
			Log.d(tag, caller + msg);
			break;
		case Log.INFO:
			Log.i(tag, caller + msg);
			break;
		case Log.ERROR:
			Log.e(tag, caller + msg);
			break;
		case Log.WARN:
			Log.w(tag, caller + msg);
			break;
		case Log.VERBOSE:
			Log.v(tag, caller + msg);
			break;
		case Log.ASSERT:
			Log.println(0, tag, caller + msg);
			break;
		default:
				break;
		}
	}
	
	public static void e(String msg){
		String caller = getCallerInfo();
		print(TAG, Log.ERROR, caller, msg);
	}
	
	public static void d(String msg){
		String caller = getCallerInfo();
		print(TAG, Log.DEBUG, caller, msg);
	}
	
	public static void i(String msg){
		String caller = getCallerInfo();
		print(TAG, Log.INFO, caller, msg);
	}
	
	public static void v(String msg){
		String caller = getCallerInfo();
		print(TAG, Log.VERBOSE, caller, msg);
	}
	
	public static void w(String msg){
		String caller = getCallerInfo();
		print(TAG, Log.WARN, caller, msg);
	}
	
	public static void println(String msg){
		String caller = getCallerInfo();
		print(TAG, Log.ASSERT, caller, msg);
	}
	
	private static String getCallerInfo(){
		StackTraceElement el = new Throwable().fillInStackTrace().getStackTrace()[2];
		String className = el.getClassName();
		String info = className.substring(className.lastIndexOf(".") + 1) + "(" + el.getLineNumber() + "): ";
		return info;
	}

}
