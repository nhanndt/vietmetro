package com.nhanndt.vietmetro.utils;

import java.util.ArrayList;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageView;

import com.nhanndt.vietmetro.VietMetro;
import com.nhanndt.vietmetro.data.MetroDBOpenHelper;
import com.nhanndt.vietmetro.data.MetroDatabaseContract;
import com.nhanndt.vietmetro.data.MetroDatabaseContract.StationTable;
import com.nhanndt.vietmetro.data.Station;

public class TouchMetroMapView extends ImageView implements OnTouchListener{
	
	public static final int STATION_MARK_RADIUS = 15; // in dp.
	public static final int LINE_WIDTH = 15; // in dp.
	
	public static final int CANCEL = -1;
	public static final int NONE = 0;
	public static final int DRAG = 1;
	public static final int ZOOM = 2;
	public int				state = NONE;
	
	private Context mContext;
	private OnStationTouchListener mOnStationTouchListener = null;
	private PointF mapRoot = new PointF();
	
	/**
	 * for Touch event detection
	 */
	private float oldDist = 0f;
	private float newDist = 0f;	
	private Matrix savedMatrix = new Matrix();
	private Matrix savedMatrix2 = new Matrix();
	private Matrix newMatrix = new Matrix();
	private PointF startPoint = new PointF();
	private PointF midPoint = new PointF();
	private float scale = 1.0f;
	private float crrtZoomScale = scale;
	private float minScale = 0.5f;
	private float maxScale = 3.0f;
	private int viewWidth;
	private int viewHeight;
	private ScaleType mScalseType = null;
	
	/**
	 * for Drawing object
	 */
	Paint markPaint = null;
	Paint pathPaint = null;
	Paint mShadowPaint = null;
	float mTextHeight = 20;
	boolean isStartSet = false;
	boolean isEndSet = false;
	boolean isDrawPath = false;
	PointF sPoint = null;
	PointF ePoint = null;
	ArrayList<Station> findingPath = null;
	ObjectAnimator colorAnimator = null;
	/**
	 * For route searching
	 */
	MetroDBOpenHelper mMetroDBOpenHelper = null;
	
	public TouchMetroMapView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		this.mContext = context;
		initImage();
	}
	
	public TouchMetroMapView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		this.mContext = context;
		initImage();
	}
	
	public TouchMetroMapView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		// TODO Auto-generated constructor stub
		this.mContext = context;
		initImage();
	}
	
	public TouchMetroMapView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		// TODO Auto-generated constructor stub
		this.mContext = context;
		initImage();
	}
	
	public void setOnStationTouchListener(OnStationTouchListener listener){
		this.mOnStationTouchListener = listener;
	}
	
	public abstract interface OnStationTouchListener{
		public void onStationTouch(Station stn);
		public void onIntersectionTouch();
	}
	
	private void initImage(){
		setOnTouchListener(this);
		mScalseType = getScaleType();
		markPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		pathPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		if(VietMetro.getCurrentCityID(mContext) == VietMetro.ID_HCM)
			mMetroDBOpenHelper = new MetroDBOpenHelper(mContext, MetroDatabaseContract.DATABASE_NAME_HCM);
		else if(VietMetro.getCurrentCityID(mContext) == VietMetro.ID_HANOI)
			mMetroDBOpenHelper = new MetroDBOpenHelper(mContext, MetroDatabaseContract.DATABASE_NAME_HANOI);
		else if(VietMetro.getCurrentCityID(mContext) == VietMetro.ID_DANANG)
			mMetroDBOpenHelper = new MetroDBOpenHelper(mContext, MetroDatabaseContract.DATABASE_NAME_DANANG);
//		mPiePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
//		mPiePaint.setStyle(Paint.Style.FILL);
//		mPiePaint.setTextSize(mTextHeight);
//
//		mShadowPaint = new Paint(0);
//		mShadowPaint.setColor(0xaa00ffff);
//		mShadowPaint.setMaskFilter(new BlurMaskFilter(8, BlurMaskFilter.Blur.NORMAL));
		//ArgbEvaluator evaluator = new ArgbEvaluator();
        colorAnimator = ObjectAnimator.ofInt(this, "color", 0xffff0000, 0xff00ff00, 0xff0000ff);
        colorAnimator.setDuration(10000);
        colorAnimator.setRepeatCount(ValueAnimator.INFINITE);
        colorAnimator.setRepeatMode(ValueAnimator.RESTART);
//        animator.addListener(new AnimatorListener() {
//			
//			@Override
//			public void onAnimationStart(Animator animation) {
//				// TODO Auto-generated method stub
//				setColor();
//			}
//			
//			@Override
//			public void onAnimationRepeat(Animator animation) {
//				// TODO Auto-generated method stub
//				
//			}
//			
//			@Override
//			public void onAnimationEnd(Animator animation) {
//				// TODO Auto-generated method stub
//				
//			}
//			
//			@Override
//			public void onAnimationCancel(Animator animation) {
//				// TODO Auto-generated method stub
//				
//			}
//		});
	}
	
	public void setColor(int color){
		NLog.e("I am setting color: " + color);
		pathPaint.setColor(color);
		invalidate();
	}
	
	public void reset(){
		isDrawPath = false;
		isStartSet = false;
		isEndSet = false;
		invalidate();
		savedMatrix = new Matrix();
		savedMatrix2 = new Matrix();
		if(VietMetro.getCurrentCityID(mContext) == VietMetro.ID_HCM)
			mMetroDBOpenHelper = new MetroDBOpenHelper(mContext, MetroDatabaseContract.DATABASE_NAME_HCM);
		else if(VietMetro.getCurrentCityID(mContext) == VietMetro.ID_HANOI)
			mMetroDBOpenHelper = new MetroDBOpenHelper(mContext, MetroDatabaseContract.DATABASE_NAME_HANOI);
		else if(VietMetro.getCurrentCityID(mContext) == VietMetro.ID_DANANG)
			mMetroDBOpenHelper = new MetroDBOpenHelper(mContext, MetroDatabaseContract.DATABASE_NAME_DANANG);
	}	
	
	@Override
	public void setImageResource(int resId){
		super.setImageResource(resId);
	}
	
	@Override
	public void setImageBitmap(Bitmap bm){
		super.setImageBitmap(bm);
		reset();
	}
	
	@Override
	public Parcelable onSaveInstanceState(){
		NLog.e("Start onSaveInstanceState");
		return super.onSaveInstanceState();
		
//    	Bundle bundle = new Bundle();
//    	bundle.putParcelable("instanceState", super.onSaveInstanceState());
//    	bundle.putFloat("saveScale", scale);
//    	bundle.putFloat("matchViewHeight", matchViewHeight);
//    	bundle.putFloat("matchViewWidth", matchViewWidth);
//    	bundle.putInt("viewWidth", viewWidth);
//    	bundle.putInt("viewHeight", viewHeight);
//    	matrix.getValues(m);
//    	bundle.putFloatArray("savedMatrix", savedMatrix);
//    	return bundle;
	}
	
	@Override
	public void onRestoreInstanceState(Parcelable state){
		NLog.e("Start onRestoreInstanceState");
      	if (state instanceof Bundle) {
//	        Bundle bundle = (Bundle) state;
//	        normalizedScale = bundle.getFloat("saveScale");
//	        m = bundle.getFloatArray("matrix");
//	        prevMatrix.setValues(m);
//	        prevMatchViewHeight = bundle.getFloat("matchViewHeight");
//	        prevMatchViewWidth = bundle.getFloat("matchViewWidth");
//	        prevViewHeight = bundle.getInt("viewHeight");
//	        prevViewWidth = bundle.getInt("viewWidth");
//	        imageRenderedAtLeastOnce = bundle.getBoolean("imageRendered");
//	        super.onRestoreInstanceState(bundle.getParcelable("instanceState"));
//	        return;
      	}

      	super.onRestoreInstanceState(state);
	}
	
	@Override
	public void onMeasure(int widthMeasureSpec, int heightMeasureSpec){
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		int rootCoords[] = new int[2];
		getLocationOnScreen(rootCoords);
		mapRoot.set(rootCoords[0], rootCoords[1]);
		Drawable d = getDrawable();
		if(d == null){
			NLog.e("drawable is null!");
			return;
		}
		viewWidth = MeasureSpec.getSize(widthMeasureSpec);
		viewHeight = MeasureSpec.getSize(heightMeasureSpec);
		fixImageToView();
	}
	
	private void fixImageToView(){
		setScaleType(ScaleType.MATRIX);
		float scaleX = (float)getDrawable().getIntrinsicWidth()/viewWidth;
		float scaleY = (float)getDrawable().getIntrinsicHeight()/viewHeight;
		switch (mScalseType) {
		case FIT_CENTER:
			scaleX = scaleY = Math.min(scaleX, scaleY);
			break;
		case FIT_XY:
			break;
		case CENTER:
			scaleX = scaleY = 1;
			break;
		case CENTER_CROP:
			scaleX = scaleY = Math.max(scaleX, scaleY);
			break;
		case CENTER_INSIDE:
			scaleX = scaleY = Math.min(1, Math.min(scaleX, scaleY));
			break;
//		case MATRIX:
//			NLog.i("setScaleType:");
//			break;
//		case FIT_END:
//			NLog.i("setScaleType:");
//			break;
//		case FIT_START:
//			NLog.i("setScaleType:");
//			break;
			
		default:
			throw new UnsupportedOperationException("TouchImageView does not support FIT_START or FIT_END");
		}
		NLog.i("scaleX: " + scaleX);
		NLog.i("scaleY: " + scaleY);
		newMatrix.set(savedMatrix);
		newMatrix.postScale(scaleX, scaleY, 0, 0);
		float scaleWidth = scaleX*(float)getDrawable().getIntrinsicWidth();
		float scaleHeight = scaleY*(float)getDrawable().getIntrinsicHeight();
		newMatrix.postTranslate((viewWidth-scaleWidth)/2, (viewHeight-scaleHeight)/2);
		setImageMatrix(newMatrix);
		fixScaleTrans();
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub		
		switch(event.getAction() & MotionEvent.ACTION_MASK){
		case MotionEvent.ACTION_DOWN:
			savedMatrix.set(newMatrix);
			savedMatrix2.set(savedMatrix);
			state = NONE;
			startPoint.set(event.getX(), event.getY());
			break;			
		case MotionEvent.ACTION_POINTER_DOWN:
			oldDist = pointerDistance(event);
			if(oldDist >10f){
				savedMatrix.set(newMatrix);
				state = ZOOM;
				midPoint.set((event.getX(0) + event.getX(1))/2, (event.getY(0) + event.getY(1))/2);
			}
			
			break;
		case MotionEvent.ACTION_MOVE:
			if((state == NONE || state == DRAG) && pointDistance(event.getX(), event.getY(), startPoint.x, startPoint.y) > 10){
				state = DRAG;
				newMatrix.set(savedMatrix);
				newMatrix.postTranslate(event.getX() - startPoint.x, event.getY() - startPoint.y);
			} else if(state == ZOOM){
				float newDist = pointerDistance(event);
				scale = newDist/oldDist;
				newMatrix.set(savedMatrix);
				newMatrix.postScale(scale,scale, midPoint.x, midPoint.y);
			}
			break;
		case MotionEvent.ACTION_POINTER_UP:
			state = CANCEL;
			break;
		case MotionEvent.ACTION_UP:
			if(state == NONE){
				touchNavigation(startPoint.x, startPoint.y);
			}
			break;			
		}
		//
		setImageMatrix(newMatrix);
		fixScaleTrans();
		return true;
	}
	
	private void fixScaleTrans(){
		float m[] = new float[9];
		float savedValue[] = new float[9];
		newMatrix.getValues(m);
		savedMatrix2.getValues(savedValue);
		if(m[0] > maxScale){
			m[0] = m[4] = maxScale;
			m[2] = savedValue[2];
			m[5] = savedValue[5];
		} else if(m[0] < minScale){
			m[0] = m[4] = minScale;
			m[2] = savedValue[2];
			m[5] = savedValue[5];
		}
		if(m[2] > Math.max(0,(viewWidth - getDrawable().getIntrinsicWidth()*m[0])))
			m[2] = Math.max(0,(viewWidth - getDrawable().getIntrinsicWidth()*m[0]));
		if(m[2] < Math.min(0, (viewWidth - getDrawable().getIntrinsicWidth()*m[0])))
			m[2] = Math.min(0, (viewWidth - getDrawable().getIntrinsicWidth()*m[0]));
		if(m[5] > Math.max(0,(viewHeight - getDrawable().getIntrinsicHeight()*m[4])))
			m[5] = Math.max(0, (viewHeight - getDrawable().getIntrinsicHeight()*m[4]));
		if(m[5] < Math.min(0, (viewHeight - getDrawable().getIntrinsicHeight()*m[4])))
			m[5] = Math.min(0, (viewHeight - getDrawable().getIntrinsicHeight()*m[4]));
		//
		newMatrix.setValues(m);
		setImageMatrix(newMatrix);
		savedMatrix2.set(newMatrix);
		crrtZoomScale = m[0];
	}
	
	public void setMaxScale(float max){
		this.maxScale = max;
	}
	
	public void setMinScale(float min){
		this.minScale = min;
	}
	
	private float pointerDistance(MotionEvent event){
		float distX = event.getX(0)- event.getX(1);
		float distY = event.getY(0) - event.getY(1);
		return (float) Math.sqrt(distX*distX + distY*distY);
	}
	private float pointDistance(float x1, float y1, float x2, float y2){
		float distX = x1 - x2;
		float distY = y1 - y2;
		return (float) Math.sqrt(distX*distX + distY*distY);
	}
	
	private void touchNavigation(float x, float y){
		Station stn = new Station();
		float minDist = VietMetro.STATION_RADIUS;
		PointF cPoint = convertToDrawbleSpace(x, y);
		mMetroDBOpenHelper.openDB();
		Cursor cursor = mMetroDBOpenHelper.loadAllStation();
		boolean movable = cursor.moveToFirst();
		while(movable){
			float sx = cursor.getFloat(cursor.getColumnIndex(MetroDatabaseContract.StationTable.COLUMN_STATION_X));
			float sy = cursor.getFloat(cursor.getColumnIndex(MetroDatabaseContract.StationTable.COLUMN_STATION_Y));
			if(pointDistance(cPoint.x, cPoint.y, sx, sy) < minDist){
				stn.x = sx;
				stn.y = sy;
				stn.stnID = cursor.getString(cursor.getColumnIndex(MetroDatabaseContract.StationTable.COLUMN_STATION_ID));
				stn.name = cursor.getString(cursor.getColumnIndex(MetroDatabaseContract.StationTable.COLUMN_STATION_NAME));
				mOnStationTouchListener.onStationTouch(stn);
				break;
			}
			movable = cursor.moveToNext();
		}
		mMetroDBOpenHelper.closeDB();
	}
	
	private PointF convertToDrawbleSpace(float x, float y){
		float savedValue[] = new float[9];
		savedMatrix.getValues(savedValue);
		PointF out = new PointF();
		out.x = (x-savedValue[2])/savedValue[0]*VietMetro.ORIGINAL_MAP_WIDTH/getDrawable().getIntrinsicWidth();
		out.y = (y-savedValue[5])/savedValue[4]*VietMetro.ORIGINAL_MAP_HEIGHT/getDrawable().getIntrinsicHeight();
		return out;		
	}
	//
	private PointF convertDrawableToScreenSpace(float x, float y){
		float savedValue[] = new float[9];
		savedMatrix.getValues(savedValue);
		PointF out = new PointF();
		out.x = x*savedValue[0]*getDrawable().getIntrinsicWidth()/1725 + savedValue[2];
		out.y = y*savedValue[4]*getDrawable().getIntrinsicHeight()/857 + savedValue[5];
		return out;		
	}
	//
	private PointF convertScreenSpaceSpace(float x, float y){
		float savedValue[] = new float[9];
		savedMatrix2.getValues(savedValue);
		PointF out = new PointF();
		out.x = x*getDrawable().getIntrinsicWidth()*savedValue[0]/VietMetro.ORIGINAL_MAP_WIDTH + savedValue[2];
		out.y = y*getDrawable().getIntrinsicHeight()*savedValue[4]/VietMetro.ORIGINAL_MAP_HEIGHT + savedValue[5];
		NLog.i("x: " + x);
		NLog.i("y: " + y);
		NLog.i("out.x: " + out.x);
		NLog.i("out.y: " + out.y);
		return out;		
	}
	
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
	}
	
	@Override
	public void onDraw(Canvas canvas){
		super.onDraw(canvas);
		canvas.save();
		PointF cPoint;
		if(isDrawPath){
			if(!colorAnimator.isRunning())
				colorAnimator.start();
			mMetroDBOpenHelper.openDB();
			//pathPaint.setColor(Color.CYAN);
			for(int i=0; i<findingPath.size(); i++){
				Cursor c = mMetroDBOpenHelper.getStationByID(findingPath.get(i).stnID);
				c.moveToFirst();
				cPoint = convertScreenSpaceSpace(c.getFloat(c.getColumnIndex(StationTable.COLUMN_STATION_X)), c.getFloat(c.getColumnIndex(StationTable.COLUMN_STATION_Y)));
				canvas.drawCircle(cPoint.x, cPoint.y, ImageTool.dpToPx(STATION_MARK_RADIUS, mContext)*crrtZoomScale, pathPaint);
//				if(i>0){
//					c = mMetroDBOpenHelper.getStationByID(findingPath.get(i-1).stnID);
//					c.moveToFirst();
//					PointF prePoint = convertScreenSpaceSpace(c.getFloat(c.getColumnIndex(StationTable.COLUMN_STATION_X)), c.getFloat(c.getColumnIndex(StationTable.COLUMN_STATION_Y)));
//					pathPaint.setStrokeWidth(ImageTool.dpToPx(LINE_WIDTH, mContext)*crrtZoomScale);
//					canvas.drawLine(cPoint.x, cPoint.y, prePoint.x, prePoint.y, pathPaint);
//				}
			}
			mMetroDBOpenHelper.closeDB();
		} else {
			if(colorAnimator.isRunning())
				colorAnimator.cancel();
		}
		if(isStartSet){
			cPoint = convertScreenSpaceSpace(sPoint.x, sPoint.y);
			markPaint.setColor(Color.GREEN);
			canvas.drawCircle(cPoint.x, cPoint.y, ImageTool.dpToPx(STATION_MARK_RADIUS, mContext)*crrtZoomScale, markPaint);
		}
		if(isEndSet){
			cPoint = convertScreenSpaceSpace(ePoint.x, ePoint.y);
			markPaint.setColor(Color.RED);
			canvas.drawCircle(cPoint.x, cPoint.y, ImageTool.dpToPx(STATION_MARK_RADIUS, mContext)*crrtZoomScale, markPaint);
		}
		canvas.restore();
	}
	
	public void drawStartPoint(float x, float y){
		isStartSet = true;
		if(sPoint == null)
			sPoint = new PointF();
		sPoint.x = x;
		sPoint.y = y;
		invalidate();
	}
	
	public void drawDestinationPoint(float x, float y){
		isEndSet = true;
		if(ePoint == null)
			ePoint = new PointF();
		ePoint.x = x;
		ePoint.y = y;
		//Bitmap endBitmap = mBitmap.copy(mBitmap.getConfig(), true);
//		Canvas canvas = new Canvas(mBitmap);
//		
//		PointF cPoint = convertToScaleDrawableSpace(ePoint.x, ePoint.y);
//		mTextPaint.setColor(0xaaff0000);
//		canvas.drawCircle(cPoint.x, cPoint.y, 30, mTextPaint);
//		if(isStartSet){
//			cPoint = convertToScaleDrawableSpace(sPoint.x, sPoint.y);
//			mTextPaint.setColor(0xaa00ff00);
//			canvas.drawCircle(cPoint.x, cPoint.y, 30, mTextPaint);
//		}
//		setImageBitmap(mBitmap);
		invalidate();
	}
	
	public void drawPath(ArrayList<Station> path){
		isDrawPath = true;
		findingPath = path;
		invalidate();
	}
	
	public void clearStartPoint(){
		isStartSet = false;
	}
	
	public void clearEndPoint(){
		isEndSet = false;
	}
	
	public boolean isSearchable(){
		return isStartSet&isEndSet;
	}
	
	public boolean isDrawn(){
		return (isStartSet||isEndSet||isDrawPath);
	}
}
