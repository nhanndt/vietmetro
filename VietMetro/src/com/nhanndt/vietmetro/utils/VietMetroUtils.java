package com.nhanndt.vietmetro.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.nhanndt.vietmetro.R;

public class VietMetroUtils {
	private static VietMetroUtils instance = null;
	
	
	private Context mContext = null;
	
	public static VietMetroUtils getInstance(Context context){
		if (instance == null)
			instance = new VietMetroUtils(context);
		return instance;
	}
	
	public VietMetroUtils(Context context){
		this.mContext = context;
	}
	public boolean isNetWorkAvailable(Context context){
		ConnectivityManager cm = (ConnectivityManager)mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
		if(cm.getActiveNetworkInfo() != null){
			NetworkInfo info = cm.getActiveNetworkInfo();
			switch (info.getType()) {
			case ConnectivityManager.TYPE_WIFI:
				return true;
			case ConnectivityManager.TYPE_MOBILE:
				return true;
			default:
				return false;
			}
		} else {
			return false;
		}
	}
	//
	public void showDialog(String content, final Activity activity, final boolean stopParentActivity){
		AlertDialog.Builder builder = new Builder(activity);
		builder.setTitle(mContext.getResources().getString(R.string.app_name));
		builder.setMessage(content);
		builder.setCancelable(false);
		builder.setPositiveButton(R.string.ok_btn_string, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				if(stopParentActivity)
					activity.finish();
			}
		});
		builder.create().show();
	}

}
